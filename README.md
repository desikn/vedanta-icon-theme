# Vedanta
An Adwaita styled theme with extra icons for popular apps to complement GNOME Shell's original icons.
The purpose of this theme is to provide a consistent look and feel with GNOME Shell's design.

This theme is built upon the work of GNOME's Adwaita designers, GNOME Circle app developers and MoreWaita and Adwaita++ theme designers with a touch of tinkering from myself.

The purpose of Vedanta is to add to Adwaita, not modify it. This theme does not override any Adwaita icons, nor any GNOME Circle app icons, nor icons that generally fit into the Adwaita paradigm. This theme is way less all-inclusive than many others, it's meant for personal use, this is a one-man hobby effort.

For most icons, especially branded ones, the general idea is to stay as close as possible to the original icons – to the point of using them in full – and giving them the distinct Adwaita 'perspective' and general flatness. One thing this theme deviates from is the GNOME colour palette in brand icons – Vedanta keeps the brand colours.

This theme is built and tested against GNOME on Debian.

## Installation

#### Download the theme
`git clone https://gitlab.com/desikn/vedanta-icon-theme.git`

#### Enter the downloaded folder
`cd vedanta-icon-theme`

#### Install system-wide (recommended)
`sudo ./install.sh`

This copies the whole theme folder into `/usr/share/icons/`. You will be prompted for your password.

#### Install local
`./install.sh`

This copies the whole theme folder into `~/.local/share/icons/`.

#### Update
Use the same steps as for installation.

#### Uninstall
Simply chose another theme and then delete the entire `Vedanta` folder from either `/usr/share/icons/` or `~/.local/share/icons/` depending on your installation choice above. 

## Activation
Either use the `Tweaks` app to choose and activate the icon theme or run the following command:

`gsettings set org.gnome.desktop.interface icon-theme 'Vedanta'`

## Using custom folder icons
1. Open Files (Nautilus).
2. Find the folder you wish to change the icon for.
3. Right click on the folder.
4. Click on `Properties`.
5. Click on the folder image.
6. Navigate to the Vedanta installation folder and into the `places` subfolder (typically `/usr/share/icons/Vedanta/scalable/places/`).
7. Select the icon you wish to use.
8. Click `Open`.
9. Follow the same procedure to revert the icon. Just click `Revert` instead of selecting a new icon in step 7.

## Troubleshooting

#### Theme doesn't apply
If the theme doesn't apply try the following command:

##### For system-wide installation
`sudo gtk-update-icon-cache -f -t /usr/share/icons/Vedanta/ && xdg-desktop-menu forceupdate`

##### For local installation
`gtk-update-icon-cache -f -t ~/.local/share/icons/Vedanta/ && xdg-desktop-menu forceupdate`

## Acknowledgments

- Vedanta is based on [MoreWaita](https://github.com/somepaulo/MoreWaita).
- Design and specifications are based on [GNOME HIG](https://developer.gnome.org/hig/).