#!/bin/sh

#cat <<- EOF

#   vv          vv
#    vv        vv              dd                    tt
#     vv      vv               dd                    tttt
#      vv    vv   eeeee    dddddd  aaaaaa    nnnnn   tt      aaaaaa
#       vv  vv   ee   ee  dd   dd  aaaaaaa  nn   nn  tt      aaaaaaa
#        vvvv    eeeeeee  dd   dd  aa   aa  nn   nn  tt      aa   aa
#         vv      eeeee    dddddd   aaaaaa  nn   nn   ttttt   aaaaaa


#EOF

is_user_root ()
{
	[ "$(id -u)" -eq 0 ]
}

if is_user_root; then
	DIR=/usr/share/icons/
	THEME_DIR=/usr/share/icons/Vedanta/
else
	DIR=$HOME/.local/share/icons/
	THEME_DIR=$HOME/.local/share/icons/Vedanta/
fi

if [ -d "$THEME_DIR" ]; then
	echo "🗑 Removing Vedanta..."
	rm -rf "$THEME_DIR"
fi

echo "🖴 Installing Vedanta..."
cp -r Vedanta "$DIR"

echo "⌦ Clearing cache..."
gtk-update-icon-cache -f -t "$THEME_DIR" && xdg-desktop-menu forceupdate

echo "✔ Done."
